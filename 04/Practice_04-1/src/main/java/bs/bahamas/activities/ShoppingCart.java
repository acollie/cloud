/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.activities;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    
    public static void main(String[] args) {
        
        String custName = "Mary Smith";
        String itemDesc = "Shirt";
        String message;
        
        message = custName + " wants to purchase a " + itemDesc;
        
        System.out.println(message);
    }
}
