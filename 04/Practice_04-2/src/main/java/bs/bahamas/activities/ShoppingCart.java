/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.activities;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    
    public static void main(String[] args) {
        
        String custName = "Mary Smith";
        String itemDesc = "Shirt";
        String message;
        double price = 23.02, tax = 0.12, total;
        int quantity = 1;
        
        total = price + (price * quantity * tax);
        message = custName + " wants to purchase " + quantity + " " + itemDesc;
        System.out.println(message);
        System.out.println("Total cost with Tax is: " + total);
    }
}
