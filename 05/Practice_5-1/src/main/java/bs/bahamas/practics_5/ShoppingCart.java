/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practics_5;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    
    public static void main(String[] args) {
        
        String custName = "Mary Smith", itemDesc = "Shirt", message;
        double price = 23.02, tax = 0.12, total;
        int quantity = 0;
        boolean outOfStock;
        
        total = price + (price * quantity * tax);
        if (quantity > 0) {
            
            outOfStock = false;
            
        } else {
            
            outOfStock = true;
        }
        
        
        if (quantity > 1) {
            message = custName + " wants to purchase " + quantity + " " + itemDesc + "s";
        } else {
            message = custName + " wants to purchase " + quantity + " " + itemDesc;
        }
        
        if (outOfStock) {
            System.out.println(itemDesc + " is out of stock");
        } else {
            System.out.println(message);
            System.out.println("Total cost with Tax is: " + total);
        }
    }
}
