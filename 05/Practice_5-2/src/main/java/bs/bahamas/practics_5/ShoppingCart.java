/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practics_5;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    
    public static void main(String[] args) {
        
        String custName = "Mary Smith", message;
        String[] items = {"Shirt", "Socks", "Scarf", "Belt"};
        
        message = custName + " wants to purchase " + items.length + " items";

        System.out.println(message);

    }
}
