/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_07;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    
    public static void main(String[] args) {
        String custName = "Mary Smith", fullName;
        int spaceIdx = custName.indexOf(" ");
        String firstName = custName.substring(0, spaceIdx), lastName = custName.substring(spaceIdx);
        StringBuilder sb = new StringBuilder(firstName);
        
        sb.append(lastName);
        
        Item item1 = new Item();
        Item item2 = new Item();
        
        item1.descr = "Shirt";
        item2.descr = "Pant";
        
        System.out.println("Item 1 is " + item1.descr);
        System.out.println("Item 2 is " + item2.descr);
        
        fullName = sb.toString();
        System.out.println(firstName + " wants to purchase " + item1.descr + " and " + item2.descr + ".");
        System.out.println(fullName + " wants to purchase " + item1.descr + " and " + item2.descr + ".");
        item1 = item2;
        
        System.out.println("Item 1 is " + item1.descr);
        System.out.println("Item 2 is " + item2.descr);
        
    }
}
