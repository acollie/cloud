/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_07;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    
    public static void main(String[] args) {
        String custName = "Mary Smith";
        int spaceIdx = custName.indexOf(" ");
        String firstName = custName.substring(0, spaceIdx);
        long longVariable = 9_000_000_000L;
        int intVariable = (int)longVariable;
        float floatVariable = 12.5F;
        char charVariable = 'S';
        
        System.out.println("long variable value: " + longVariable);
        System.out.println("int variable value: " + intVariable);
        
        Item item1 = new Item();
        Item item2 = new Item();
        
        item1.descr = "Shirt";
        item2.descr = "Pant";
        
        System.out.println("Item 1 is " + item1.descr);
        System.out.println("Item 2 is " + item2.descr);
        
        
        System.out.println(firstName + " wants to purchase " + item1.descr + " and " + item2.descr + ".");
        item1 = item2;
        
        System.out.println("Item 1 is " + item1.descr);
        System.out.println("Item 2 is " + item2.descr);
        
    }
}
