/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

/**
 *
 * @author Alexandros Collie
 */
public class League {
    
    public static void main(String[] args) {
        
        // starting game
        Game game = new Game();
        game.homeTeam = new Team();
        game.awayTeam = new Team();
        
        // creating team Greens
        game.homeTeam.name = "Greens:";
        game.homeTeam.roster = new String[3];
        
        // adding the players
        game.homeTeam.roster[0] = "George Eliot";
        game.homeTeam.roster[1] = "Graham Greene";
        game.homeTeam.roster[2] = "Geoffrey Chaucer";
        
        // creating team Reds
        game.awayTeam.name = "Reds:";
        game.awayTeam.roster = new String[3];
        
        // adding the players
        game.awayTeam.roster[0] = "Robert Service";
        game.awayTeam.roster[1] = "Robbie Burns";
        game.awayTeam.roster[2] = "Refael Sabetini";
        
        // starting play
        Goal[] goals = new Goal[3];
        
        goals[0] = new Goal();
        goals[0].playerName = game.homeTeam.roster[2];
        goals[0].teamName = game.homeTeam.name;
        goals[0].time = 12.0;
        
        goals[1] = new Goal();
        goals[1].playerName = game.homeTeam.roster[1];
        goals[1].teamName = game.homeTeam.name;
        goals[1].time = 23.0;
        
        goals[2] = new Goal();
        goals[2].playerName = game.awayTeam.roster[0];
        goals[2].teamName = game.awayTeam.name;
        goals[2].time = 55.0;
        
        // printing
        
        //Teams and lineup
        // home team
        System.out.println(game.homeTeam.name);
        
        // team
        for (String player : game.homeTeam.roster) {
            System.out.println(player);
        }
        
        // visiting team
        System.out.println();
        System.out.println(game.awayTeam.name);
        
        //team
        for (String player : game.awayTeam.roster) {
            System.out.println(player);
        }
        
        // scoring
        System.out.println();
        System.out.println("Goals: ");
        
        for (Goal goal : goals) {
            System.out.println("Goal scored after " + goal.time + " mins by " + goal.playerName + " of the " + goal.teamName);
        }
        
        System.out.println();
        
        // locating the player
        String searchResult = "", searchCriteria = ".*Sab.*";
        for (String player : game.awayTeam.roster) {
            if (player.matches(searchCriteria)) {
                searchResult = player;
                break;
            } else {
                searchResult = "";
            }
        }
        
        System.out.println("Found " + searchResult);
        
        System.out.println();
        
        //Teams and lineup
        // home team
        System.out.println(game.homeTeam.name);
        
        // team
        for (String player : game.homeTeam.roster) {
            int spaceIndex = player.indexOf(" ");
            StringBuilder reverseFullName = new StringBuilder(player.substring(spaceIndex + 1));
            reverseFullName.append(", ").append(player.substring(0, spaceIndex));
            System.out.println(reverseFullName);
        }
        
        // visiting team
        System.out.println();
        System.out.println(game.awayTeam.name);
        
        //team
        for (String player : game.awayTeam.roster) {
            int spaceIndex = player.indexOf(" ");
            StringBuilder reverseFullName = new StringBuilder(player.substring(spaceIndex + 1));
            reverseFullName.append(", ").append(player.substring(0, spaceIndex));
            System.out.println(reverseFullName.toString());
        }
    }
}
