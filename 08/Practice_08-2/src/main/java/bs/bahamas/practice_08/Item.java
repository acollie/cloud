/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_08;

/**
 *
 * @author alexandros
 */
public class Item {
    public int id;
    public String desc;
    public double price;
    public int quantity;
    public char colorCode = 'U'; // 'U' = Undetermined
    
    public void displayItem() {
        System.out.println("Description: " + desc);
        System.out.println("Quantity: " + quantity);
        System.out.println("Color code: " + colorCode);
        System.out.println("Price: " + price);
    }
    public boolean setColor(char colorCode) {
        boolean isColorCodeValid = false;
        char emptyChar;
        
        emptyChar = ' ';
        if (colorCode == emptyChar) {
            isColorCodeValid = false;
        } else {
            isColorCodeValid = true;
        }
        
        return isColorCodeValid;
    }
    
    public void setItemFields(String desc, int quantity, double price) {
        this.desc = desc;
        this.quantity = quantity;
        this.price = price;
    }
    
    public int setItemFields(String desc, int quantity, double price, char colorCode) {
        
        int returnValue = 0;
        if (colorCode == ' ') {
            returnValue = -1;
        } else {
            this.colorCode = colorCode;
            setItemFields(desc, quantity, price);
            returnValue = 1;
        }
        
        return returnValue;
    }
}
