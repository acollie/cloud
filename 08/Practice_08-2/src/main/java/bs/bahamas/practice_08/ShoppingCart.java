/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_08;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    
    public static void main(String[] args) {
        String custName = "Mary Smith", fullName;
        int spaceIdx = custName.indexOf(" ");
        String firstName = custName.substring(0, spaceIdx), lastName = custName.substring(spaceIdx);
        StringBuilder sb = new StringBuilder(firstName);
        
        sb.append(lastName);
        
        Item item1 = new Item();
        Item item2 = new Item();
        
        item1.setItemFields("Shirt", 20, 10.50);
        item1.displayItem();
        
        System.out.println();
        
        if (item2.setItemFields("Shirt", 50, 9.99, 'B') < 0) {
            System.out.println("Invalid color code. Item not added");
        } else {
            item2.displayItem();
        }
    }
}
