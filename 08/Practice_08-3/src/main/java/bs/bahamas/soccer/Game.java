/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

import bs.bahamas.utility.GameUtils;

/**
 *
 * @author Alexandros Collie
 */
public class Game {
    public Team homeTeam;
    public Team awayTeam;
    public Goal[] goals;
    
    public void playGame() {
        playGame(6);
    }
    
    public void playGame(int maximumNumberOfGoalsScored) {
        int numberOfGoals = (int) (Math.random() * maximumNumberOfGoalsScored + 1);
        Goal[] theGoals = new Goal[numberOfGoals];
        this.goals = theGoals;
        GameUtils.addGameGoals(this);
    }
    
    public String getDescription() {
        StringBuilder goalsDescriptions = new StringBuilder();
        
        for (Goal currentGoal : this.goals) {
            goalsDescriptions.append("Goal scored after ")
                                    .append(currentGoal.theTime)
                                    .append(" minutes by ")
                                    .append(currentGoal.thePlayer.name)
                                    .append(" of ")
                                    .append(currentGoal.theTeam.name)
                                    .append("\n");
        }
        
        return goalsDescriptions.toString();
    }
}
