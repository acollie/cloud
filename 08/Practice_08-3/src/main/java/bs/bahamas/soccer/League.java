/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

/**
 *
 * @author Alexandros Collie
 */
public class League {
    
    public static void main(String[] args) {
        League theLeague = new League();
        
        
        // creating teams
        Team[] teams = theLeague.createTeam();
        
        Game[] games = theLeague.createGame(teams);
        
        Game currentGame = games[0];
        currentGame.playGame();
        System.out.println(currentGame.getDescription());
        
    }
    
    public Team[] createTeam() {
        Team[] teams = new Team[2];
        
        // create team 1
        Player player1 = new Player();
        player1.name = "George Eliot";
        Player player2 = new Player();
        player2.name = "Graham Greene";
        Player player3 = new Player();
        player3.name = "Geoffrey Chaucer"; 
        Player[] thePlayers = {player1, player2, player3};
        teams[0] = new Team();
        teams[0].name = "The Greens";
        teams[0].roster = thePlayers;
        
        // creating team 2
        teams[1] = new Team();
        teams[1].name = "The Reds";
        teams[1].roster = new Player[3];
        teams[1].roster[0] = new Player();
        teams[1].roster[0].name = "Robert Service";
        teams[1].roster[1] = new Player();
        teams[1].roster[1].name = "Robbie Burns";
        teams[1].roster[2] = new Player();
        teams[1].roster[2].name = "Refael Sabetini";
        
        return teams;
    }
    
    public Game[] createGame(Team[] teams) {
        Game[] games = new Game[1];
        
        games[0] = new Game();
        games[0].homeTeam = teams[0];
        games[0].awayTeam = teams[1];
        
        games[0].goals = new Goal[3];
        games[0].goals[0] = new Goal();
        
        games[0].goals[0].thePlayer = games[0].homeTeam.roster[2];
        games[0].goals[0].theTeam = games[0].homeTeam;
        games[0].goals[0].theTime = 12;
        
        games[0].goals[1] = new Goal();
        games[0].goals[1].thePlayer = games[0].homeTeam.roster[1];
        games[0].goals[1].theTeam = games[0].awayTeam;
        games[0].goals[1].theTime = 23;
        
        games[0].goals[2] = new Goal();
        games[0].goals[2].thePlayer = games[0].homeTeam.roster[0];
        games[0].goals[2].theTeam = games[0].awayTeam;
        games[0].goals[2].theTime = 55;
        
        return games;
    }
}
