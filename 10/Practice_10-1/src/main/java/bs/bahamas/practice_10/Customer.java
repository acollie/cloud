/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_10;

/**
 *
 * @author Alexandros Collie
 */
public class Customer {
    private String name;
    private String ssn;
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public String getSsn() {
        return ssn;
    }
}
