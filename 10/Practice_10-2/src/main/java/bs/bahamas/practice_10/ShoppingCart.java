/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_10;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    public static void main(String[] args){
        Customer customer;
        customer = new Customer("Tom Criuze", "555-44-3212");
        
        // Print the customer object name.
        System.out.println("The customer name is " + customer.getName());
    }
}
