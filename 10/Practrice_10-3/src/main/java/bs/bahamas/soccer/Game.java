/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

import bs.bahamas.utility.GameUtils;

/**
 *
 * @author Alexandros Collie
 */
public class Game {
    private Team homeTeam;
    private Team awayTeam;
    private Goal[] goals;
    
    public Game(Team homeTeam, Team awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }
    
    /**
     * @return the homeTeam
     */
    public Team getHomeTeam() {
        return homeTeam;
    }

    /**
     * @param homeTeam the homeTeam to set
     */
    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    /**
     * @return the awayTeam
     */
    public Team getAwayTeam() {
        return awayTeam;
    }

    /**
     * @param awayTeam the awayTeam to set
     */
    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    /**
     * @return the goals
     */
    public Goal[] getGoals() {
        return goals;
    }
    
    
    /**
     * @param goals the goals to set
     */
    public void setGoals(Goal[] goals) {
        this.goals = goals;
    }
    
    public void playGame() {
        playGame(6);
    }
    
    public void playGame(int maximumNumberOfGoalsScored) {
        int numberOfGoals = (int) (Math.random() * maximumNumberOfGoalsScored + 1);
        Goal[] theGoals = new Goal[numberOfGoals];
        this.setGoals(theGoals);
        GameUtils.addGameGoals(this);
    }
    
    public String getDescription() {
        StringBuilder goalsDescriptions = new StringBuilder();
        
        for (Goal currentGoal : this.getGoals()) {
            goalsDescriptions.append("Goal scored after ")
                                    .append(currentGoal.getTheTime())
                                    .append(" minutes by ")
                                    .append(currentGoal.getThePlayer().getName())
                                    .append(" of ")
                                    .append(currentGoal.getTheTeam().getName())
                                    .append("\n");
        }
        
        return goalsDescriptions.toString();
    }
}
