/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

/**
 *
 * @author Alexandros Collie
 */
public class League {
    
    public static void main(String[] args) {
        League theLeague = new League();
        
        
        // creating teams
        Team[] teams = theLeague.createTeam();
        
        Game[] games = theLeague.createGame(teams);
        
        for (Game currentGame: games) {
            currentGame.playGame();
            System.out.println(currentGame.getDescription());
        }
        
    }
    
    public Team[] createTeam() {
        Team[] teams = new Team[2];
        
        // create team 1
        Player player1 = new Player("George Eliot");
        Player player2 = new Player("Graham Greene");
        Player player3 = new Player("Geoffrey Chaucer");
        Player[] thePlayers = {player1, player2, player3};
        teams[0] = new Team("The Greens", thePlayers);
        
        // creating team 2
        teams[1] = new Team("The Reds", new Player[3]);
        teams[1].getRoster()[0] = new Player("Robert Service");
        teams[1].getRoster()[1] = new Player("Robbie Burns");
        teams[1].getRoster()[2] = new Player("Refael Sabetini");
        
        return teams;
    }
    
    public Game[] createGame(Team[] teams) {
        Game[] games = new Game[4];
        
        games[0] = new Game(teams[0], teams[1]);
        games[1] = new Game(teams[1], teams[0]);
        games[2] = new Game(teams[0], teams[1]);
        games[3] = new Game(teams[1], teams[0]);
        
        return games;
    }
}
