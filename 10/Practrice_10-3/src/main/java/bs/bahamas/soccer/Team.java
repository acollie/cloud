/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

/**
 *
 * @author Alexandros Collie
 */
public class Team {

    private Player[] roster;
    private String name;
    
    public Team(String name, Player[] roster) {
        this.name = name;
        this.roster = roster;
    }
    
    /**
     * @return the roster
     */
    public Player[] getRoster() {
        return roster;
    }

    /**
     * @param roster the roster to set
     */
    public void setRoster(Player[] roster) {
        this.roster = roster;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
}
