/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

/**
 *
 * @author Alexandros Collie
 */
public class League {
    
    public static void main(String[] args) {
        League theLeague = new League();
        int[] gamesWon = new int[2];
        int[] pointsAwarded = {0, 0};
        
        
        // creating teams
        Team[] teams = theLeague.createTeam();
        
        Game[] games = theLeague.createGame(teams);
        
        for (Game game : games) {
            int[] goalsScored = {0, 0};
            System.out.println(game.getAwayTeam().getName() + " vs." + game.getHomeTeam().getName());
            game.playGame();
            System.out.println(game.getDescription());
            
            for (Goal goal : game.getGoals()) {
                if (goal.getTheTeam().getName().equals(teams[0].getName())) {
                    goalsScored[0]++;
                } else {
                    goalsScored[1]++;
                }
            }
            
            teams[0].setGoalsScored(teams[0].getGoalsScored() + goalsScored[0]);
            teams[1].setGoalsScored(teams[1].getGoalsScored() + goalsScored[1]);
            
            if (goalsScored[0] > goalsScored[1]) {
                System.out.println(teams[0].getName() + " wins (" + goalsScored[0] + " - " + goalsScored[1] + ")");
                pointsAwarded[0] += 2; 
            } else if (goalsScored[0] < goalsScored[1]) {
                System.out.println(teams[1].getName() + " wins (" + goalsScored[1] + " - " + goalsScored[0] + ")");
                pointsAwarded[1] += 2;
            } else {
                System.out.println("It's a draw! (" + goalsScored[1] + " - " + goalsScored[0] + ")");
                pointsAwarded[0]++;
                pointsAwarded[1]++;
            }
            
            teams[0].setPointsEarned(pointsAwarded[0]);
            teams[1].setPointsEarned(pointsAwarded[1]);
            System.out.println();
        }
        
        System.out.println();
        System.out.println("Team Points");
        for (Team team : teams ) {
            System.out.println(team.getName() + " : " + team.getPointsEarned() + " : " + team.getGoalsScored());
        }
        
        if (teams[0].getPointsEarned() > teams[1].getPointsEarned()) {
            System.out.println("This year's champions are : " + teams[0].getName() + "!");
        } else if (teams[0].getPointsEarned() < teams[1].getPointsEarned()) {
            System.out.println("This year's champions are : " + teams[1].getName() + "!");
        } else {
            if (teams[0].getGoalsScored() > teams[1].getGoalsScored()) {
                System.out.println("This year's champions are : " + teams[0].getName() + "!");
            } else if (teams[0].getGoalsScored() < teams[1].getGoalsScored()) {
                System.out.println("This year's champions are : " + teams[1].getName() + "!");
            } else {
                System.out.println("This year's champions are : ");
                for (Team team : teams) 
                    System.out.println(team.getName());
                
            }
        }
        
    }
    
    public Team[] createTeam() {
        Team[] teams = new Team[2];
        
        // create home team
        Player player1 = new Player("George Eliot");
        Player player2 = new Player("Graham Greene");
        Player player3 = new Player("Geoffrey Chaucer");
        Player[] thePlayers = {player1, player2, player3};
        teams[0] = new Team("The Greens", thePlayers);
        
        // creating visiting team
        teams[1] = new Team("The Reds", new Player[3]);
        teams[1].getRoster()[0] = new Player("Robert Service");
        teams[1].getRoster()[1] = new Player("Robbie Burns");
        teams[1].getRoster()[2] = new Player("Refael Sabetini");
        
        return teams;
    }
    
    public Game[] createGame(Team[] teams) {
        Game[] games = new Game[4];
        
        games[0] = new Game(teams[0], teams[1]);
        games[1] = new Game(teams[1], teams[0]);
        games[2] = new Game(teams[0], teams[1]);
        games[3] = new Game(teams[1], teams[0]);
        
        return games;
    }
}
