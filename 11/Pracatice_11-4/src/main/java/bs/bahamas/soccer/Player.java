/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

/**
 *
 * @author alexandros
 */
public class Player {

    private String name;
    private int numberOfGoalsScored;
    
    public Player(String name) {
        this.name = name;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the numberOfGoalsScored
     */
    public int getNumberOfGoalsScored() {
        return numberOfGoalsScored;
    }

    /**
     * @param numberOfGoalsScored the numberOfGoalsScored to set
     */
    public void setNumberOfGoalsScored(int numberOfGoalsScored) {
        this.numberOfGoalsScored = numberOfGoalsScored;
    }
}
