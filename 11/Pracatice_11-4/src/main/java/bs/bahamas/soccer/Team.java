/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

/**
 *
 * @author Alexandros Collie
 */
public class Team {

    private Player[] roster;
    private String name;
    private int pointsEarned;
    private int goalsScored;
    
    public Team(String name, Player[] roster) {
        this.name = name;
        this.roster = roster;
    }
    
    /**
     * @return the roster
     */
    public Player[] getRoster() {
        return roster;
    }

    /**
     * @param roster the roster to set
     */
    public void setRoster(Player[] roster) {
        this.roster = roster;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the pointsEarned
     */
    public int getPointsEarned() {
        return pointsEarned;
    }

    /**
     * @param pointsEarned the pointsEarned to set
     */
    public void setPointsEarned(int pointsEarned) {
        this.pointsEarned = pointsEarned;
    }

    /**
     * @return the goalsScored
     */
    public int getGoalsScored() {
        return goalsScored;
    }

    /**
     * @param goalsScored the goalsScored to set
     */
    public void setGoalsScored(int goalsScored) {
        this.goalsScored = goalsScored;
    }
    
}
