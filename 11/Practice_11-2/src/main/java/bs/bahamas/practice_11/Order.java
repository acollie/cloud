/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_11;

/**
 *
 * @author alexandros
 */
public class Order {
    static final char CORP = 'C';
    static final char PRIVATE = 'P';
    static final char NONPROFIT = 'N';
    
    String name;
    double total;
    String stateCode;
    double discount;
    char custType;
    
    public Order(String name, double total, String state, char custType) {
        this.name = name;
        this.total = total;
        this.stateCode = state;
        this.custType = custType;
        calcDiscount();
    }

    private void calcDiscount() {
        if (custType == NONPROFIT && total > 900.00) {
            discount = 10.0;
        } else if (custType == NONPROFIT && total <= 900.00) {
            discount = 5.0;
        } else  if (custType == PRIVATE && total > 900.00){
            discount = 7.0;
        } else if (custType == NONPROFIT && total <= 900.00) {
            discount = 0.0;
        } else if (custType == CORP && total > 900.00) {
            discount = 8.0;
        } else if (custType == CORP && total <= 900.00) {
            discount = 5.0;
        } else {
            System.out.println("Invalid Customer Type");
        }
    }
    
    public String getDiscount() {
        return Double.toString(discount) + "%";
    }
}
