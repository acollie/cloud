/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

import bs.bahamas.utility.GameUtils;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Alexandros Collie
 */
public class Game {
    private Team homeTeam;
    private Team awayTeam;
    private Goal[] goals;
    private LocalDateTime theDatetime;
    
    public Game(Team homeTeam, Team awayTeam, LocalDateTime theDateTime) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.theDatetime = theDateTime;
    }
    
    /**
     * @return the homeTeam
     */
    public Team getHomeTeam() {
        return homeTeam;
    }

    /**
     * @param homeTeam the homeTeam to set
     */
    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    /**
     * @return the awayTeam
     */
    public Team getAwayTeam() {
        return awayTeam;
    }

    /**
     * @param awayTeam the awayTeam to set
     */
    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    /**
     * @return the goals
     */
    public Goal[] getGoals() {
        return goals;
    }
    
    
    /**
     * @param goals the goals to set
     */
    public void setGoals(Goal[] goals) {
        this.goals = goals;
    }
    
    public void playGame() {
        playGame(6);
    }
    
    public void playGame(int maxGoals) {
        int numberOfGoals = (int)(Math.random() * maxGoals +1);
        Goal[] theGoals = new Goal[numberOfGoals];
        this.goals = theGoals;
        GameUtils.addGameGoals(this);
    }
    
    public String getDescription() {
        int homeTeamGoals = 0;
        int awayTeamGoals = 0;
        
        StringBuilder goalsDescriptions = new StringBuilder();
        
        goalsDescriptions.append(this.getHomeTeam().getName())
                         .append(" vs ")
                         .append(this.getAwayTeam().getName())
                         .append("\nDate: ")
                         .append(this.getTheDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE))
                         .append("\n");
        
        for (Goal currentGoal : this.getGoals()) {
            if (currentGoal.getTheTeam() == homeTeam) {
                homeTeamGoals++;
                homeTeam.incGoalsTotal(1);
            } else {
                awayTeamGoals++;
                awayTeam.incGoalsTotal(1);
            }
            
            goalsDescriptions.append("Goal scored after ")
                             .append(currentGoal.getTheTime())
                             .append(" minutes by ")
                             .append(currentGoal.getThePlayer().getName())
                             .append(" of ")
                             .append(currentGoal.getTheTeam().getName())
                             .append("\n");
        }
        
        if (homeTeamGoals == awayTeamGoals) {
            goalsDescriptions.append("It's a draw!");
            this.homeTeam.incPointsTotal(1);
            this.awayTeam.incPointsTotal(1);
        } else if (homeTeamGoals > awayTeamGoals) {
            goalsDescriptions.append(homeTeam.getName())
                             .append(" wins");
            this.homeTeam.incPointsTotal(2);
        } else {
            goalsDescriptions.append(awayTeam.getName())
                             .append(" wins");
            this.awayTeam.incPointsTotal(2);
        }
        
        goalsDescriptions.append(" (")
                         .append(homeTeamGoals)
                         .append(" - ")
                         .append(awayTeamGoals)
                         .append(") \n");
        
        return goalsDescriptions.toString();
    }

    public LocalDateTime getTheDateTime() {
        return this.theDatetime;
    }
    
    public void setTheDateTime(LocalDateTime theDateTime) {
        this.theDatetime = theDateTime;
    }
}
