/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

import bs.bahamas.utility.PlayerDatabase;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author Alexandros Collie
 */
public class League {
    
    public static void main(String[] args) {
        League theLeague = new League();
        
        
        // creating theTeams
        Team[] teams = theLeague.createTeam("The Robins,The Crows,The Swallows", 5);
        Game[] games = theLeague.createGame(teams);
        
        System.out.println(theLeague.getLeagueAnnouncement(games));
        for (Game currGame : games) {
            currGame.playGame();
            System.out.println(currGame.getDescription());
        }
        
        theLeague.showBestTeam(teams);
        
    }
    
    public Team[] createTeam(String teamNames, int teamSize) {
        PlayerDatabase playerDB = new PlayerDatabase();
        
        StringTokenizer teamNameTokens = new StringTokenizer(teamNames, ",");
        Team[] theTeams = new Team[teamNameTokens.countTokens()];
        for (int i = 0; i < theTeams.length; i++) {
            theTeams[i] = new Team(teamNameTokens.nextToken(), playerDB.getTeam(teamSize));
        }

        
        return theTeams;
    }
    
    public Game[] createGame(Team[] teams) {
        int daysBetweenGames = 0;
        ArrayList games = new ArrayList();
        
        for (Team homeTeam: teams) {
            for (Team awayTeam: teams) {
                if (homeTeam != awayTeam) {
                    daysBetweenGames += 7;
                    games.add(new Game(homeTeam, awayTeam, LocalDateTime.now().plusDays(daysBetweenGames)));
                }
            }
        }
        
        return (Game[]) games.toArray(new Game[1]);
    }
    
    public void showBestTeam(Team[] teams) {
        Team currBestTeam = new Team("Dummy Team");
        String currBestTeamName = currBestTeam.getName();
        System.out.println("\nTeam Points");
        
        for (Team currTeam: teams) {
            System.out.println(currTeam.getName() + " : " 
                                                  + currTeam.getPointsEarned() 
                                                  + " : " 
                                                  + currTeam.getGoalsScored());
            
            if (currTeam.getPointsEarned() > currBestTeam.getPointsEarned()) {
                currBestTeam = currTeam;
                currBestTeamName = currBestTeam.getName();
            } else if (currTeam.getPointsEarned() == currBestTeam.getPointsEarned()) {
                if (currTeam.getGoalsScored() > currBestTeam.getGoalsScored()) {
                    currBestTeam = currTeam;
                    currBestTeamName = currBestTeam.getName();
                } else if (currTeam.getGoalsScored() == currBestTeam.getGoalsScored()) {
                    currBestTeamName += ", " + currTeam.getName();
               }
            }
        }
        
        System.out.println("This year's champions are: " + currBestTeamName + "!");
    }
    
    public String getLeagueAnnouncement(Game[] games) {
        Period period = Period.between(games[0].getTheDateTime().toLocalDate()
                                       , games[games.length - 1].getTheDateTime().toLocalDate());
        
        return "The league is scheduled to run for " 
                + period.getMonths() 
                + " month(s), and " 
                + period.getDays() 
                + " day(s)\n";
        
    }
}
