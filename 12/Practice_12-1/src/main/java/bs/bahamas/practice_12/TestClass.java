/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_12;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Alexandros Collie
 */
public class TestClass {
    
    public static void main(String[] args) {
        // Inistialize the LocalDateTime object and print it
        LocalDateTime orderDate = LocalDateTime.now();
        System.out.println("The order date (unformatted) is " + orderDate);
        
        // Format the object using ISO_LOCAL_DATE and print it
        System.out.println("The order date (formatted) is " + orderDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
    }
}
