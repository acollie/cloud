/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_12;

/**
 *
 * @author Alexandros Collie
 */
public class TestClass {
    
    public static void main(String[] args) {
        
        if (args.length >= 2) {
            System.out.println("Name: " + args[0] + "\n" + "Age: " + Integer.parseInt(args[1]));
        } else {
            System.out.println("Two arguments are required!");
        }
    }
}
