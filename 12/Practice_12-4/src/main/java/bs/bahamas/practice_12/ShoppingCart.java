/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_12;

import java.util.ArrayList;

/**
 *
 * @author alexandros
 */
public class ShoppingCart {
    
    public static void main(String[] args) {
        
        // Declare, instantiate, and intialize an ArrayList of Strings. Print and test code
        ArrayList<String> elementList = new ArrayList();
        elementList.add("Naofumi");
        elementList.add("Filo");
        elementList.add("Raphtalia");
        
        System.out.println("The Characters of The Rising of the Shiled Hero are:");
        
        elementList.forEach((element) -> {
            System.out.println(element);
        });
        
        // add (insert) another element at a specific index
        elementList.add(1, "Melty");
        System.out.println();
        System.out.println("The Characters of The Rising of the Shiled Hero are:");
        
        elementList.forEach((element) -> {
            System.out.println(element);
        }); 

        // Check for the existence of a specific String element.
        // if it exists, remove it.
        if (elementList.contains("Filo")) {
            elementList.remove("Filo");
        }
        
        System.out.println();
        System.out.println("The Characters of The Rising of the Shiled Hero are:");
        elementList.forEach((element) -> {
            System.out.println(element);
        });
    }
}
