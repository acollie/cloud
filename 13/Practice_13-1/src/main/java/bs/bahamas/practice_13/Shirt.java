/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_13;

/**
 *
 * @author Alexandros Collie
 */
public class Shirt extends Item {
    private char size;
    private char colorCode;
    
    public Shirt(double price, char size, char colorCode) {
        super("Shirt", price);
        setSize(size);
        setColorCode(colorCode);
    }
    
    private void setSize(char size) {
        this.size = size;
    }

    private void setColorCode(char colorCode) {
        this.colorCode = colorCode;
    }
    
    /**
     * Overrides the display method in the Item Class
     */
    @Override
    public void display() {
        super.display();
        System.out.println("\tSize: " + size);
        System.out.println("\tColor: " + colorCode);
    }
    
}
