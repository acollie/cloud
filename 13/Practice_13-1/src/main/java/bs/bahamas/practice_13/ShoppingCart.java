/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_13;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    public static void main(String[] args) {
        // instantiate a shirt object and call display() on the object reference
        Shirt shirt = new Shirt(12.50, 'L', 'R');
        
        shirt.display();
        
        Shirt tShirt = new Shirt(5.25, 'S', 'W');
        tShirt.display();
    }
}
