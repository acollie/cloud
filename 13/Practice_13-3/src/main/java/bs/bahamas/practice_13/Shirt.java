/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_13;

/**
 *
 * @author Alexandros Collie
 */
public class Shirt extends Item {
    private char size;
    private char colorCode;
    
    public Shirt(double price, char size, char colorCode) {
        super("Shirt", price);
        setSize(size);
        setColorCode(colorCode);
    }
    
    private void setSize(char size) {
        this.size = size;
    }

    private void setColorCode(char colorCode) {
        this.colorCode = colorCode;
    }
    
    public String getColor() {
        switch(colorCode) {
            case 'R' :
              return "Red";
            case 'B' :
              return "Blue";
            case 'G' :
                return "Green";
            case 'W' :
                return "White";
            case 'Y':
                return "Yellow";
            default:
                return "Invalid  code";
        }
    }
    
    
    
}
