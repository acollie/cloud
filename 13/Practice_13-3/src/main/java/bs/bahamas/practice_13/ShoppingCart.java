/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_13;

/**
 *
 * @author Alexandros Collie
 */
public class ShoppingCart {
    public static void main(String[] args) {
        // instantiate a shirt object and call display() on the object reference
        Item shirt = new Shirt(12.50, 'L', 'R');
        
        if (shirt instanceof Shirt) {
            shirt.display();
            System.out.println("\tColor: " + ((Shirt) shirt).getColor());
        } else {
            System.out.println("Item is not a shirt");
        }
        
        Item tShirt = new Shirt(5.25, 'S', 'W');
        
        if (tShirt instanceof Shirt) {
            tShirt.display();
            System.out.println("\tColor: " + ((Shirt) tShirt).getColor());
        } else {
            System.out.println("Item is not a shirt");
        }
    }
}
