/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.practice_14;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Alexandros Collie
 */
public class TestClass {
    public static void main(String[] args) {
        String[] days = {"monday","saturday","tuesday","sunday","friday"};

        // Convert the days array into an ArrayList
        // Loop through the ArrayList, printing out "sunday" elements in 
        //   upper case (use toUpperCase() method of String class) 
        // Print all other days in lower case 
        // Print out the ArrayList  
        
        System.out.println("Days of the week:");
        ArrayList<String> daysArrayList = new ArrayList(Arrays.asList(days));
        daysArrayList.forEach((String day) -> {
            if (day.equals("sunday")) {
                System.out.println(day.toUpperCase());
            } else {
                System.out.println(day);
            }
        });
        
        System.out.println();
        System.out.println(daysArrayList);
    } 
}
