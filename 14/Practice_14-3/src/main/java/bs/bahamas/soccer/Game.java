/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author Alexandros Collie
 */
public class Game {
    private Team homeTeam;
    private Team awayTeam;
    private GameEvent[] events;
    private LocalDateTime theDatetime;
    
    public Game(Team homeTeam, Team awayTeam, LocalDateTime theDateTime) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.theDatetime = theDateTime;
    }
    
    /**
     * @return the homeTeam
     */
    public Team getHomeTeam() {
        return homeTeam;
    }

    /**
     * @param homeTeam the homeTeam to set
     */
    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    /**
     * @return the awayTeam
     */
    public Team getAwayTeam() {
        return awayTeam;
    }

    /**
     * @param awayTeam the awayTeam to set
     */
    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    /**
     * @return the events
     */
    public GameEvent[] getEvents() {
        return events;
    }
    
    
    /**
     * @param events the events to set
     */
    public void setEvents(GameEvent[] events) {
        this.events = events;
    }
    
    public void playGame() {
        ArrayList<GameEvent> eventList = new ArrayList();
        GameEvent currentEvent;
        
        for (int i = 1; i <= 90; i++) {
            
            if (Math.random() > 0.95) {
                currentEvent = Math.random() > 0.67 ? new Goal(): new Possession();
                currentEvent.setTeam(Math.random() > 0.3 ? homeTeam: awayTeam);
                currentEvent.setPlayer(currentEvent.getTeam()
                                                   .getRoster()[(int)(Math.random() * currentEvent.getTeam().getRoster().length)]);
                currentEvent.setTime(i);
                eventList.add(currentEvent);
            }
            
            this.events = new GameEvent[eventList.size()];
            eventList.toArray(events);
        }
    }
    
    public String getDescription() {
        int homeTeamGoals = 0;
        int awayTeamGoals = 0;
        
        StringBuilder goalsDescriptions = new StringBuilder();
        
        goalsDescriptions.append(this.getHomeTeam().getName())
                         .append(" vs ")
                         .append(this.getAwayTeam().getName())
                         .append("\nDate: ")
                         .append(this.getTheDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE))
                         .append("\n");
        
        for (GameEvent currentGameEvent : this.getEvents()) {
            if (currentGameEvent.toString().equalsIgnoreCase("Goal")){
                if (currentGameEvent.getTeam() == homeTeam) {
                    homeTeamGoals++;
                    homeTeam.incGoalsTotal(1);
                } else {
                    awayTeamGoals++;
                    awayTeam.incGoalsTotal(1);
                }
            }
            goalsDescriptions.append(currentGameEvent)
                             .append(currentGameEvent.toString().equalsIgnoreCase("Possession") ? " changed after " : " after ")
                             .append(currentGameEvent.getTime())
                             .append(" minutes by ")
                             .append(currentGameEvent.getPlayer().getName())
                             .append(" of ")
                             .append(currentGameEvent.getTeam().getName())
                             .append("\n");
        }
        
        if (homeTeamGoals == awayTeamGoals) {
            goalsDescriptions.append("It's a draw!");
            this.homeTeam.incPointsTotal(1);
            this.awayTeam.incPointsTotal(1);
        } else if (homeTeamGoals > awayTeamGoals) {
            goalsDescriptions.append(homeTeam.getName())
                             .append(" wins");
            this.homeTeam.incPointsTotal(2);
        } else {
            goalsDescriptions.append(awayTeam.getName())
                             .append(" wins");
            this.awayTeam.incPointsTotal(2);
        }
        
        goalsDescriptions.append(" (")
                         .append(homeTeamGoals)
                         .append(" - ")
                         .append(awayTeamGoals)
                         .append(") \n");
        
        return goalsDescriptions.toString();
    }

    public LocalDateTime getTheDateTime() {
        return this.theDatetime;
    }
    
    public void setTheDateTime(LocalDateTime theDateTime) {
        this.theDatetime = theDateTime;
    }
}
