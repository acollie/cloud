/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bs.bahamas.soccer;

import bs.bahamas.utility.PlayerDatabase;
import bs.bahamas.utility.PlayerDatabaseException;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author Alexandros Collie
 */
public class League {
    
    public static void main(String[] args) {
        League league = new League();
        
        
        // creating teams
        try {
            Team[] teams = league.createTeam("The Robins,The Crows,The Swallows,The Eagles,The Penguins", 8);
            Game[] games = league.createGame(teams);

            System.out.println(league.getLeagueAnnouncement(games));
            for (Game currGame : games) {
                currGame.playGame();
                System.out.println(currGame.getDescription());
            }

            league.showBestTeam(teams);
        } catch(PlayerDatabaseException e) {
            e.printStackTrace();
        }
        
    }
    
    public Team[] createTeam(String teamNames, int teamSize) throws PlayerDatabaseException {
        PlayerDatabase playerDB = new PlayerDatabase();
        
        StringTokenizer teamNameTokens = new StringTokenizer(teamNames, ",");
        Team[] teams = new Team[teamNameTokens.countTokens()];
        for (int i = 0; i < teams.length; i++) {
            teams[i] = new Team(teamNameTokens.nextToken(), playerDB.getTeam(teamSize));
        }

        
        return teams;
    }
    
    public Game[] createGame(Team[] teams) {
        int daysBetweenGames = 0;
        ArrayList games = new ArrayList();
        
        for (Team homeTeam: teams) {
            for (Team awayTeam: teams) {
                if (homeTeam != awayTeam) {
                    daysBetweenGames += 7;
                    games.add(new Game(homeTeam, awayTeam, LocalDateTime.now().plusDays(daysBetweenGames)));
                }
            }
        }
        
        return (Game[]) games.toArray(new Game[1]);
    }
    
    public void showBestTeam(Team[] teams) {
        Team currBestTeam = new Team("Dummy Team");
        String currBestTeamName = currBestTeam.getName();
        System.out.println("\nTeam Points");
        
        for (Team currTeam: teams) {
            System.out.println(currTeam.getName() + " : " 
                                                  + currTeam.getPointsEarned() 
                                                  + " : " 
                                                  + currTeam.getGoalsScored());
            
            if (currTeam.getPointsEarned() > currBestTeam.getPointsEarned()) {
                currBestTeam = currTeam;
                currBestTeamName = currBestTeam.getName();
            } else if (currTeam.getPointsEarned() == currBestTeam.getPointsEarned()) {
                if (currTeam.getGoalsScored() > currBestTeam.getGoalsScored()) {
                    currBestTeam = currTeam;
                    currBestTeamName = currBestTeam.getName();
                } else if (currTeam.getGoalsScored() == currBestTeam.getGoalsScored()) {
                    currBestTeamName += ", " + currTeam.getName();
               }
            }
        }
        
        System.out.println("This year's champions are: " + currBestTeamName + "!");
    }
    
    public String getLeagueAnnouncement(Game[] games) {
        Period period = Period.between(games[0].getTheDateTime().toLocalDate()
                                       , games[games.length - 1].getTheDateTime().toLocalDate());
        
        return "The league is scheduled to run for " 
                + period.getMonths() 
                + " month(s), and " 
                + period.getDays() 
                + " day(s)\n";
        
    }
}
